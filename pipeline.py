import os
import logging

import toml

from util.common import pipeline


module_logger = logging.getLogger(__name__)


def main():

    parsed = pipeline.create_parser(
        description="Run PST RFI test case assessment pipeline").parse_args()

    log_level = logging.INFO
    if parsed.verbose:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level)
    logging.getLogger("partialize").setLevel(logging.ERROR)
    logging.getLogger("matplotlib").setLevel(logging.ERROR)

    if parsed.measure_input_dir is None:
        parsed.measure_input_dir = parsed.output_dir

    config_file_path = parsed.config_file

    if os.path.exists(config_file_path):
        with open(config_file_path, "r") as fd:
            config = toml.load(fd)
    else:
        raise RuntimeError(f"Can't locate {config_file_path}")

    pipeline.run_pipeline(
        measure_only=parsed.measure_only,
        data_processing_only=parsed.data_processing_only,
        results_file_path=parsed.plot,
        measure_input_dir=parsed.measure_input_dir,
        output_dir=parsed.output_dir,
        config=config
    )


if __name__ == "__main__":
    main()
