import asyncio

from mpi4py import MPI


async def some_awaitable(idx):
    await asyncio.sleep(4.0)
    print(f"idx={idx}")


def main():
    comm = MPI.COMM_WORLD

    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank == 0:
        indices = list(range(size))
    else:
        indices = None

    idx = comm.scatter(indices, root=0)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(some_awaitable(idx))


if __name__ == "__main__":
    main()
