from __future__ import print_function

import psrchive
import numpy as np


file_paths = [
    "products/iridium/skzm-128__skzs-3/skzm-128__skzs-3_0.total",
    "products/iridium/vanilla/vanilla_0.total"
]

archives = []

indices = np.concatenate((
    np.arange(418, 420),
    np.arange(380, 402),
    np.arange(415, 417)
))

for file_path in file_paths:
    archive = psrchive.Archive_load(file_path)
    archive.pscrunch()
    data = archive.get_data()

    for subint in range(data.shape[0]):
        freq_slice = data[subint, 0, indices, :]
        zeros = np.zeros(freq_slice.shape)
        nclose = np.mean(np.isclose(freq_slice, zeros))
        print("file_path={}, subint={}, nclose={}".format(
            file_path, subint, nclose))
