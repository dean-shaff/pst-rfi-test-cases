## 0.1.0
- Initial Version

## 0.2.0
- Continuous RFI now uses true and false positive rates, in addition to using dump files intead of output archives.
- streamlined configuration files; `[data]` section is bundled into `[processing]`
- no separate directories for `continuous` and `impulsive` RFI test case configurations.
