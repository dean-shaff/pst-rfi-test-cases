#!/usr/bin/env bash

# ./make_op_plot.bash ADSB 64 4 1080 1100
# ./make_op_plot.bash DME 256 5 1125 1160
# ./make_op_plot.bash iridium 1024 3 1615 1630
# ./make_op_plot.bash iridium 8192 3 1615 1630
# ./make_op_plot.bash terrestrial 8192 3 930 950
# ./make_op_plot.bash satellite 8192 3 1555 1595
# ./make_op_plot.bash all 128 3


subdir=$1
skzm=$2
skzs=$3

freq0=$4
freq1=$5

sk_name="skzm-${skzm}__skzs-${skzs}"

vanilla_subdir="./products/${subdir}/vanilla"
vanilla_filename="vanilla_0.total"
sk_subdir="./products/${subdir}/${sk_name}"
sk_filename="${sk_name}_0.total"

if [ -z ${freq0} ]; then
  psrplot -p freq+ "${vanilla_subdir}/${vanilla_filename}" -jpT -D /PNG && mv pgplot.png "products/${subdir}/vanilla.png"
  psrplot -p freq+ "${sk_subdir}/${sk_filename}" -jpT -D /PNG && mv pgplot.png "products/${subdir}/${sk_name}.png"
else
  psrplot -p freq+ "${vanilla_subdir}/${vanilla_filename}" -jpT -c y:win=${freq0}:${freq1} -D /PNG && mv pgplot.png "products/${subdir}/vanilla.png"
  psrplot -p freq+ "${sk_subdir}/${sk_filename}" -jpT -c y:win=${freq0}:${freq1} -D /PNG && mv pgplot.png "products/${subdir}/${sk_name}.png"
fi
