import argparse
import typing
import os

import numpy as np
import psr_formats
import matplotlib.pyplot as plt


def measure_confusion_rates(
    excised_profile: np.ndarray,
    rfi_profile: np.ndarray
) -> typing.Tuple[np.ndarray, np.ndarray, float, float]:
    """

    Any numpy arrays that are provided should be of the following shape:
        ``(ndat, nchan, npol)``

    Args:
        rfi_profile (np.ndarray): The original RFI profile. Should be single
            polarization
        excised_profile (np.ndarray): Some data that have the RFI profile
            added and later excised via some processing
            (like DSPSR Spectral Kurtosis)

    """
    def calc_confusion(rfi, data):

        npol = data.shape[-1]
        rfi_mask = rfi != 0
        rfi_mask = np.repeat(rfi_mask, npol, axis=2)
        no_rfi_mask = np.logical_not(rfi_mask)

        true_positive = np.sum(data[rfi_mask] == 0)
        true_negative = np.sum(data[no_rfi_mask] != 0)

        false_positive = np.sum(data[no_rfi_mask] == 0)
        false_negative = np.sum(data[rfi_mask] != 0)

        if (
            true_positive + true_negative + false_positive + false_negative
            != data.size
        ):
            raise ValueError("Sum of confusion matrix must be number of elements in data array")

        return {
            "tp": true_positive,
            "tn": true_negative,
            "fp": false_positive,
            "fn": false_negative
        }

    def calc_true_positive(rfi, data):
        npol = data.shape[-1]
        mask = rfi != 0
        mask = np.repeat(mask, npol, axis=2)
        rfi_zone = data[mask]

        rfi_zapped = np.isclose(rfi_zone, np.zeros(rfi_zone.shape))
        true_positives = np.sum(rfi_zapped)
        positives = np.sum(mask)
        true_positive_rate = true_positives / positives
        return true_positive_rate

    def calc_false_positive(rfi, data):
        npol = data.shape[-1]
        mask = rfi == 0
        mask = np.repeat(mask, npol, axis=2)

        non_rfi_zone = data[mask]

        non_rfi_zapped = np.isclose(non_rfi_zone, np.zeros(non_rfi_zone.shape))
        false_positives = np.sum(non_rfi_zapped)
        negatives = np.sum(mask)
        false_positive_rate = false_positives / negatives

        return false_positive_rate

    min_size = min([rfi_profile.shape[0], excised_profile.shape[0]])

    excised_profile = excised_profile[:min_size, :, :]
    rfi_profile = rfi_profile[:min_size, :, :]

    tp = calc_true_positive(rfi_profile, excised_profile)
    fp = calc_false_positive(rfi_profile, excised_profile)

    return excised_profile, rfi_profile, tp, fp


def plot_confusion_rates(
    excised_profile: np.ndarray,
    rfi_profile: np.ndarray,
    tp: float,
    fp: float,
    plot_rfi_profile: bool = True,
    chunk_size: float = 1.0,
    ipol: int = 0
) -> tuple:

    imshow_kwargs = dict(
        aspect="auto",
        origin="lower")

    dump_ax_title = (f"Excised pulsar profile, polarization {ipol}: \n"
                     f"True positive rate = {tp*100:.4f} %\n"
                     f"False positive rate = {fp*100:.4f} %\n")

    size = excised_profile.shape[0]
    chunk = int(chunk_size * size)
    figsize = (8, 6)

    if plot_rfi_profile:
        fig, axes = plt.subplots(2, 1, sharey=True, figsize=figsize)

        axes[0].imshow(np.abs(excised_profile[:chunk, :, ipol]).T, **imshow_kwargs)
        axes[0].set_title(dump_ax_title, fontsize="x-large")

        axes[1].imshow(np.abs(rfi_profile[:chunk, :, ipol]).T, **imshow_kwargs)
        axes[1].set_title("RFI Profile", fontsize="x-large")
        axes[1].set_xlabel("Sample Number", fontsize="large")

        ax = fig.add_subplot(111, frameon=False)
        ax.tick_params(labelcolor='none', top=False, bottom=False, left=False, right=False)
        ax.set_ylabel("Channel Number", fontsize="large")
    else:
        fig, ax = plt.subplots(1, 1, figsize=figsize)

        ax.imshow(np.abs(excised_profile[:chunk, :, ipol]).T, **imshow_kwargs)
        ax.set_title(dump_ax_title, fontsize="x-large")
        ax.set_xlabel("Sample Number", fontsize="large")
        ax.set_ylabel("Channel Number", fontsize="large")

    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    return fig, axes


def create_parser() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
        description="Plot a post SK dump file with associated RFI profile")

    parser.add_argument("file_paths", metavar="file-paths", type=str, nargs="+",
                        help="Specify the post SK dump file and the RFI profile")

    parser.add_argument("-c", "--chunk-size", type=float,
                        default=1.0,
                        dest="chunk_size",
                        help="Specify the percentage of the time domain to plot")

    parser.add_argument("--no-rfi-profile", action="store_true",
                        dest="no_rfi_profile",
                        help="Don't plot the RFI profile")

    return parser


def main():

    parsed = create_parser().parse_args()

    file_paths = parsed.file_paths

    data_arrays = [psr_formats.DADAFile(fp).load_data().data for fp in file_paths]

    products = measure_confusion_rates(*data_arrays)
    fig, axes = plot_confusion_rates(
        *products,
        chunk_size=parsed.chunk_size,
        plot_rfi_profile=not parsed.no_rfi_profile)

    dump_file_path = file_paths[0]
    dump_file_name = os.path.basename(dump_file_path)

    plot_file_path = os.path.splitext(dump_file_path)[0] + ".png"

    fig.suptitle(f"{dump_file_name}", fontsize="x-large")

    fig.savefig(plot_file_path)
    plt.show()


if __name__ == "__main__":
    main()
