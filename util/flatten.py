import typing


def flatten(*lists) -> typing.Generator:
    """
    Flatten a list of lists recursively.
    """

    def _flatten(lists, prev=None):
        if prev is None:
            prev = tuple()

        if len(lists) == 1:
            for item in lists[0]:
                yield prev + (item, )
        else:
            for item in lists[0]:
                yield from _flatten(lists[1:], prev + (item, ))

    for item in _flatten(lists):
        yield item
