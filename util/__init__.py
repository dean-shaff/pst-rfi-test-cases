import os
import typing
import glob
import subprocess
import shlex
import asyncio

import dspsr_test


__all__ = [
    "get_cmd_output",
    "combine_archives",
    "get_archive_files",
    "get_sub_dir_files_hdr",
    "get_sub_dir_files_dada"
]


async def get_cmd_output(cmd_str, stderr=None):
    proc = await asyncio.create_subprocess_exec(*shlex.split(cmd_str),
                                                stdout=subprocess.PIPE,
                                                stderr=stderr)
    out, err = await proc.communicate()
    return out


def get_sub_dir_files_dada(sub_dir: str) -> typing.Generator:
    glob_pattern = os.path.join(sub_dir, "*.dada")
    file_names = glob.glob(glob_pattern)
    file_names = sorted(file_names)
    for file_name in file_names:
        yield file_name


def get_sub_dir_files_hdr(sub_dir: str) -> typing.Generator:
    glob_pattern = os.path.join(sub_dir, "*.hdr")
    file_names = glob.glob(glob_pattern)
    file_names = sorted(file_names)
    for file_name in file_names:
        yield file_name


def get_archive_files(folder: str) -> typing.Generator:
    glob_pattern = os.path.join(folder, "*.ar")
    file_names = glob.glob(glob_pattern)
    file_names = sorted(file_names)
    for file_name in file_names:
        yield file_name


def combine_archives(
    ar_paths: typing.List[str],
    max_size: float = 500e6,
    **kwargs
) -> typing.List[str]:
    """
    """
    output_template = kwargs.get("output_template", "")

    def get_split_index(paths):

        total_size = 0
        idx = 0
        while idx < len(paths):
            total_size += os.path.getsize(ar_paths[idx])
            if total_size >= max_size:
                break
            idx += 1
        return idx

    awaitables = []

    i_awaitable = 0

    while len(ar_paths) > 0:
        idx = get_split_index(ar_paths)
        kwargs["output_template"] = f"{output_template}_{i_awaitable}"
        awaitable = dspsr_test.run_psradd(
            ar_paths[:idx], **kwargs)
        ar_paths = ar_paths[idx:]
        awaitables.append(awaitable)
        i_awaitable += 1

    return awaitables
