import logging

import numpy as np

module_logger = logging.getLogger(__name__)

__all__ = [
    # "calc_confusion",
    "calc_true_positive",
    "calc_false_positive"
]


# def calc_confusion(rfi: np.ndarray, data: np.ndarray) -> dict:
#
#     npol = data.shape[-1]
#     rfi_mask = rfi != 0
#     rfi_mask = np.repeat(rfi_mask, npol, axis=2)
#     no_rfi_mask = np.logical_not(rfi_mask)
#
#     true_positive = np.sum(data[rfi_mask] == 0)
#     true_negative = np.sum(data[no_rfi_mask] != 0)
#
#     false_positive = np.sum(data[no_rfi_mask] == 0)
#     false_negative = np.sum(data[rfi_mask] != 0)
#
#     if (
#         true_positive + true_negative + false_positive + false_negative
#         != data.size
#     ):
#         raise ValueError("Sum of confusion matrix must be number of elements in data array")
#
#     return {
#         "tp": true_positive,
#         "tn": true_negative,
#         "fp": false_positive,
#         "fn": false_negative
#     }


def calc_true_positive(rfi: np.ndarray, data: np.ndarray, **kwargs) -> float:
    npol = data.shape[-1]
    mask = rfi != 0
    mask = np.repeat(mask, npol, axis=2)
    rfi_zone = data[mask]

    rfi_zapped = np.isclose(rfi_zone, np.zeros(rfi_zone.shape), **kwargs)
    true_positives = np.sum(rfi_zapped)
    positives = np.sum(mask)
    module_logger.debug(
        f"calc_true_positive: true_positives={true_positives}, positives={positives}")
    true_positive_rate = true_positives / positives
    return true_positive_rate


def calc_false_positive(rfi: np.ndarray, data: np.ndarray, **kwargs) -> float:
    npol = data.shape[-1]
    mask = rfi == 0
    mask = np.repeat(mask, npol, axis=2)

    non_rfi_zone = data[mask]

    non_rfi_zapped = np.isclose(non_rfi_zone, np.zeros(non_rfi_zone.shape), **kwargs)
    false_positives = np.sum(non_rfi_zapped)
    negatives = np.sum(mask)
    false_positive_rate = false_positives / negatives

    return false_positive_rate
