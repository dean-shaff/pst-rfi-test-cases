import logging
import asyncio
import typing

import numpy as np
from . import chain, get_cmd_output

__all__ = [
    "slice_freq"
    # "calc_snr"
]

module_logger = logging.getLogger(__name__)


def generate_psrsh_snr_script(
    on_pulse: typing.List[typing.List[float]],
    file_path: str = None
) -> str:
    """
    Returns:
        str: file path of newly generated psrsh snr script
    """
    on_pulse = sorted(on_pulse, key=lambda item: item[0])

    on_range = ",".join([f"{val[0]}:{val[1]}" for val in on_pulse])

    off_range = ":".join([
        "0.0",
        ":".join([f"{val[0]},{val[1]}" for val in on_pulse]),
        "1.0"
    ])

    script = f"""#!/usr/bin/env psrstat -C
#
# snr.psh
#

# set the signal-to-noise tratio estimator to the module
snr=modular

snr:off=set
snr:off:range={off_range}

snr:on=set
snr:on:range={on_range}
"""

    module_logger.debug(f"generate_psrsh_snr_script: script={script}")

    if file_path is None:
        file_path = "snr.psh"

    with open(file_path, "w") as fd:
        fd.write(script)

    return file_path


async def generate_pazi_cmd(
    ar_file: str, freq_start: float, freq_end: float
) -> str:
    """
    Generate a ``pazi`` command that will isolate the frequencies between
    ``freq_start`` and ``freq_end``.
    """
    module_logger.debug(
        (f"generate_pazi_cmd ar_file={ar_file}, "
         f"freq_start={freq_start}, freq_end={freq_end}"))

    def format_psredit_output(output):
        module_logger.debug(f"generate_pazi_cmd.format_psredit_output: output={output}")
        return (output.split("=")[-1]).strip()

    nchan_cmd = f"psredit {ar_file} -c nchan"
    freq_cmd = f"psredit {ar_file} -c freq"
    bw_cmd = f"psredit {ar_file} -c bw"

    module_logger.debug(f"generate_pazi_cmd: nchan_cmd={nchan_cmd}")
    module_logger.debug(f"generate_pazi_cmd: freq_cmd={freq_cmd}")
    module_logger.debug(f"generate_pazi_cmd: bw_cmd={bw_cmd}")

    output = await asyncio.gather(
        get_cmd_output(nchan_cmd),
        get_cmd_output(freq_cmd),
        get_cmd_output(bw_cmd)
    )

    chained = chain.chain(
        float, format_psredit_output, lambda val: val.decode("utf-8"))
    output = [chained(val) for val in output]
    module_logger.debug(f"generate_pazi_cmd: output={output}")
    nchan, freq, bw = output
    nchan = int(nchan)
    bw_2 = bw/2
    channels = np.arange(nchan)
    freqs = np.linspace(freq - bw_2, freq + bw_2, nchan)
    delta_freq = freqs[-1] - freqs[-2]
    mask = np.logical_or(
        freqs < freq_start - delta_freq, freqs > freq_end)
    channels_masked = channels[mask].astype(int).tolist()
    channels_masked_str = [str(chan) for chan in channels_masked]

    pazi_cmd = (f"paz -z \"{' '.join(channels_masked_str)}\" "
                f"-e pazi {{}}")

    return pazi_cmd


def freq_to_chan(archive, freq):
    """
    Convert a frequency into a channel index
    """
    freqs = archive.get_frequencies()
    indices = np.arange(len(freqs))
    return indices[freqs >= freq][0]


def slice_freq(ar, freq_start, freq_end):
    """
    Get the part of an archive between two frequency ranges
    """
    ar = ar.clone()
    freqs = ar.get_frequencies()
    ichan_start = np.arange(len(freqs))[freqs < freq_start][-1]
    ar.remove_chan(0, ichan_start - 1)
    freqs = ar.get_frequencies()
    ichan_start = np.arange(len(freqs))[freqs > freq_end][0]
    ar.remove_chan(ichan_start + 1, len(freqs) - 1)
    return ar
