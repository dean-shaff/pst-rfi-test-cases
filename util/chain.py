import typing


def chain(*funcs: typing.List[callable]) -> callable:

    def _chained(*args):
        res = funcs[-1](*args)
        for func in funcs[:-1][::-1]:
            res = func(res)
        return res

    return _chained
