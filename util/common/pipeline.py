import typing
import argparse
import logging
import os
import asyncio
import shutil
import glob
import functools
import json

import numpy as np
import dspsr_test
import tqdm
import matplotlib.pyplot as plt

import util
import util.psrchive_util

from . import continuous, impulsive


module_logger = logging.getLogger(__name__)


async def process_single(
    data_file_paths: typing.List[str],
    options: str,
    output_template: str,
    output_dir: str,
    dspsr_bin: str,
    dump_stage: str = None
) -> typing.List[str]:

    # os.chdir(output_dir)
    after_cmd = None
    if dump_stage is not None:
        options += f" -dump {dump_stage}"
        dump_file_name = f"pre_{dump_stage}.{output_template}.dump"
        dump_file_path = os.path.join(output_dir, dump_file_name)
        after_cmd_str = f"mv pre_{dump_stage}.dump {dump_file_path}"

        async def after_cmd():
            await util.get_cmd_output(after_cmd_str)
            return dump_file_path

    runner = dspsr_test.run_dspsr(
        options, dspsr_bin=dspsr_bin,
        multiple_output=True,
        output_template=output_template,
        output_dir=output_dir
    )

    ar_file_names, _ = await runner(data_file_paths)
    if hasattr(ar_file_names, "format"):
        ar_file_names = [ar_file_names]
    module_logger.debug(f"process_single: ar_file_names={ar_file_names}")
    ar_file_paths = ar_file_names
    # ar_file_paths = [os.path.join(output_dir, file_name)
    #                  for file_name in ar_file_names]

    combined = await asyncio.gather(
        *util.combine_archives(
            ar_file_paths,
            output_template=output_template,
            max_size=500e6,
            output_dir=output_dir))

    if after_cmd is not None:
        combined.append(await after_cmd())

    module_logger.debug(f"process_single: combined={combined}")

    return combined


def data_processing_pipeline(
    output_dir: str,
    config: dict
) -> None:

    processing_config = config["processing"]
    default_options = processing_config["options"]
    if "time_range" in processing_config:
        time_range = processing_config["time_range"]
        time_interval = processing_config["time_interval"]

        default_options = (f"{default_options} -S {time_range[0]} "
                           f"-T {time_range[1] - time_range[0]} "
                           f"-L {time_interval}")

    dspsr_bin = config["dspsr_bin"]
    base_data_dir = config["data_dir"]
    if "file_name" not in config:
        data_file_paths = list(util.get_sub_dir_files_dada(base_data_dir))
    else:
        data_file_paths = [os.path.join(base_data_dir, config["file_name"])]

    dump_stage = processing_config.get("dump_stage", None)

    m_vals = processing_config["skzm"]
    s_vals = processing_config["skzs"]

    skz_common_config_params = [
        "skz_no_fscr",
        "skz_no_tscr",
        "skz_no_ft"
    ]
    module_logger.debug(f"data_processing_pipeline: config[\"processing\"]={processing_config}")
    skz_common_config = []
    for param in skz_common_config_params:
        if param in processing_config:
            module_logger.debug(f"data_processing_pipeline: {param} in config[\"processing\"]")
            if processing_config[param]:
                skz_common_config.append(f"-{param}")

    skz_common_config_str = " ".join(skz_common_config)
    module_logger.debug(
        f"data_processing_pipeline: skz_common_config_str={skz_common_config_str}")

    def create_dspsr_awaitable(output_template, options):

        output_subdir = os.path.join(output_dir, output_template)

        if not os.path.exists(output_subdir):
            os.makedirs(output_subdir)

        awaitable = process_single(
            data_file_paths,
            options,
            output_template,
            output_subdir,
            dspsr_bin,
            dump_stage=dump_stage
        )

        return awaitable

    awaitables = []

    awaitables.append(
        create_dspsr_awaitable("vanilla", default_options))

    for m_val, s_val in util.flatten.flatten(m_vals, s_vals):
        output_template = f"skzm-{m_val}__skzs-{s_val}"
        options = (f"{default_options} -skz -skzm {m_val} "
                   f"-skzs {s_val} {skz_common_config_str}")
        awaitable = create_dspsr_awaitable(output_template, options)
        awaitables.append(awaitable)

    loop = asyncio.get_event_loop()
    for awaitable in tqdm.tqdm(awaitables, desc="data_processing_pipeline"):
        loop.run_until_complete(awaitable)


def measurement_pipeline(
    input_dir: str,
    output_dir: str,
    config: dict,
    create_measure_awaitable: typing.Callable,
    json_file_name: str = "measure.json"
) -> str:

    processing_config = config["processing"]

    m_vals = processing_config["skzm"]
    s_vals = processing_config["skzs"]

    awaitables = [create_measure_awaitable("vanilla", input_dir, output_dir)]
    output_templates = ["vanilla"]

    for m_val, s_val in util.flatten.flatten(m_vals, s_vals):
        output_template = f"skzm-{m_val}__skzs-{s_val}"
        awaitable = create_measure_awaitable(
            output_template, input_dir, output_dir)
        if awaitable:
            awaitables.append(awaitable)
            output_templates.append(output_template)

    loop = asyncio.get_event_loop()
    # ret = loop.run_until_complete(asyncio.gather(*awaitables))
    ret = []
    for awaitable in tqdm.tqdm(awaitables, desc="measurement_pipeline"):
        ret.append(loop.run_until_complete(awaitable))

    json_file_path = os.path.join(output_dir, json_file_name)

    with open(json_file_path, "w") as fd:
        obj = {
            output_templates[idx]: ret[idx]
            for idx in range(len(output_templates))
        }
        json.dump(obj, fd)

    return json_file_path


def plot_measurement_results(
    results_file_path: str,
    val_formatter: typing.Callable = None
) -> typing.Generator:

    if val_formatter is None:
        def val_formatter(val):
            return f"{val:.2f}"

    results_file_name = os.path.basename(results_file_path)
    results_dir_name = os.path.basename(os.path.dirname(results_file_path))

    plot_file_name = (f"{results_dir_name}."
                      f"{os.path.splitext(results_file_name)[0]}.png")

    plot_file_path = os.path.join(
        os.path.dirname(results_file_path), plot_file_name)

    module_logger.debug(
        f"plot_measurement_results: plot_file_path={plot_file_path}")

    def parse_key(key):
        vals = [int(val.split("-")[-1]) for val in key.split("__")]
        return vals

    with open(results_file_path, "r") as fd:
        results = json.load(fd)

    vanilla_val = results["vanilla"]

    dat = []
    m_vals = []
    s_vals = []

    for key in results:
        try:
            m_val, s_val = parse_key(key)
        except ValueError:
            continue
        if hasattr(results[key], "__getitem__"):
            dat.append([m_val, s_val, [
                results[key][idx] - vanilla_val[idx] for idx in range(len(vanilla_val))]])
        else:
            dat.append([m_val, s_val, [results[key] - vanilla_val]])
        m_vals.append(m_val)
        s_vals.append(s_val)

    m_vals = sorted(list(set(m_vals)))
    s_vals = sorted(list(set(s_vals)))
    n_s = len(s_vals)
    n_m = len(m_vals)

    dat_sorted_m = sorted(dat, key=lambda item: item[0])

    dat_2d = []

    for idx in range(n_m):
        chunk = dat_sorted_m[n_s*idx:n_s*(idx+1)]
        chunk = sorted(chunk, key=lambda item: item[1])
        dat_2d.append(
            np.array([item[-1] for item in chunk]))

    dat_2d = np.array(dat_2d).transpose()
    module_logger.debug(f"plot_measurement_results: dat_2d.shape={dat_2d.shape}")
    naxes = dat_2d.shape[0]

    fig, ax = plt.subplots(1, naxes, figsize=(10, 6))
    if not hasattr(ax, "__getitem__"):
        ax = [ax]

    for iaxes in range(naxes):
        ax[iaxes].imshow(dat_2d[iaxes, :, :])

        ax[iaxes].set_yticks(np.arange(n_s))
        ax[iaxes].set_ylim(n_s-0.5, -0.5)
        ax[iaxes].set_yticklabels(s_vals, fontsize="large")

        ax[iaxes].set_xticks(np.arange(n_m))
        ax[iaxes].set_xlim(-0.5, n_m-0.5)
        ax[iaxes].set_xticklabels(m_vals, fontsize="large")
        # ax[iaxes].set_xticklabels([""] + m_vals + [""], fontsize="large")

        ax[iaxes].set_xlabel("skz m value", fontsize="x-large")
        ax[iaxes].set_ylabel("skz s value", fontsize="x-large")

        for sdx in range(n_s):
            for mdx in range(n_m):
                # print(dat_2d[iaxes, sdx, mdx])
                ax[iaxes].text(mdx, sdx, val_formatter(dat_2d[iaxes, sdx, mdx]),
                               ha="center", va="center", color="w")
        if not hasattr(vanilla_val, "__getitem__"):
            ax[iaxes].set_title(
                (f"Vanilla value = {val_formatter(vanilla_val)}\n"
                 f"{os.path.join(results_dir_name, results_file_name)}"), fontsize="xx-large")

    yield fig, ax

    fig.savefig(plot_file_path)

    yield


def run_pipeline(
    measure_only: bool,
    data_processing_only: bool,
    output_dir: str,
    config: dict,
    results_file_path: str = None,
    measure_input_dir: str = None
) -> None:

    data_processing_pipeline_partial = functools.partial(
        data_processing_pipeline,
        output_dir=output_dir,
        config=config)

    measurement_pipeline_partial = functools.partial(
        measurement_pipeline,
        output_dir=output_dir,
        config=config)

    def measure_impulsive_real_data_factory(measure_single_func):

        def measure_impulsive_real_data(output_template, input_dir, output_dir):
            sub_dir = os.path.join(input_dir, output_template)
            try:
                ar_file_path = glob.glob(f"{sub_dir}/*.total")[0]
                if input_dir != output_dir:
                    ar_file_name = os.path.basename(ar_file_path)
                    output_subdir = os.path.join(output_dir, output_template)
                    if not os.path.exists(output_subdir):
                        os.makedirs(output_subdir)
                    ar_file_path_output = os.path.join(output_subdir, ar_file_name)
                    module_logger.debug(
                        (f"measurement_pipeline: copying {ar_file_path} "
                         f"to {ar_file_path_output}"))
                    shutil.copyfile(ar_file_path, ar_file_path_output)
                    ar_file_path = ar_file_path_output
                awaitable = measure_single_func(ar_file_path)
                return awaitable
            except IndexError:
                return

        return measure_impulsive_real_data

    def measure_impulsive_simulated_data_factory(measure_single_func, rfi_profile_file_path):

        def measure_impulsive_simulated_data(output_template, input_dir, output_dir):
            sub_dir = os.path.join(input_dir, output_template)
            try:
                dump_file_path = glob.glob(f"{sub_dir}/pre_*.dump")[0]
                awaitable = measure_single_func(
                    dump_file_path, rfi_profile_file_path)
                return awaitable
            except IndexError:
                return

        return measure_impulsive_simulated_data

    def measure_continuous_real_data_factory(measure_single_func):

        def measure_continuous_real_data(output_template, input_dir, output_dir):
            sub_dir = os.path.join(input_dir, output_template)
            try:
                # dump_file_path = glob.glob(f"{sub_dir}/pre_*.dump")[0]
                ar_file_path = glob.glob(f"{sub_dir}/*.total")[0]
                awaitable = measure_single_func(ar_file_path)
                return awaitable
            except IndexError:
                return

        return measure_continuous_real_data

    axes_titles = [
        "True Positive Rate",
        "False Positive Rate"
    ]
    plot_title = "Spectral Kurtosis Performance: True Positive and False Positive Rate"

    def val_formatter(val):
        return f"{100*(val):.4f} %"

    if config["type"] == "continuous":
        def get_ranges(*names):
            freq_range = None
            for name in names:
                freq_range = config["measurement"].get(name, None)
                if freq_range is not None:
                    break

            if freq_range is None:
                raise RuntimeError(
                    (f"Need to provide one of {names} in "
                     "continuous type configuration file"))
            return freq_range

        freq_range = get_ranges("freq_range", "chan_range")
        rfi_freq_range = get_ranges("rfi_freq_range", "rfi_chan_range")

        measure_single_func = continuous.measure_single(
            freq_or_chan_range=freq_range,
            rfi_freq_or_chan_range=rfi_freq_range
        )

        measurement_pipeline_partial = functools.partial(
            measurement_pipeline_partial,
            json_file_name="measure.continuous.real.json",
            create_measure_awaitable=measure_continuous_real_data_factory(measure_single_func)
        )

    elif config["type"] == "impulsive":
        if "rfi_profile" in config["measurement"]:
            rfi_profile_file_path = os.path.join(
                config["data_dir"], config["measurement"]["rfi_profile"])

            measurement_pipeline_partial = functools.partial(
                measurement_pipeline_partial,
                json_file_name="measure.impulsive.simulated.json",
                create_measure_awaitable=measure_impulsive_simulated_data_factory(
                    impulsive.measure_confusion, rfi_profile_file_path)
            )
        else:
            axes_titles = None
            freq_start, freq_end = None, None
            if "freq_range" in config["measurement"]:
                freq_start, freq_end = config["measurement"]["freq_range"]
            on_pulse = config["measurement"]["on_pulse"]
            dm = config["processing"].get("dm", None)

            measure_single_func = impulsive.measure_single(
                freq_start=freq_start, freq_end=freq_end, on_pulse=on_pulse, dm=dm
            )

            measurement_pipeline_partial = functools.partial(
                measurement_pipeline_partial,
                json_file_name="measure.impulsive.real.json",
                create_measure_awaitable=measure_impulsive_real_data_factory(measure_single_func)
            )
            plot_title = "Spectral Kurtosis Performance: skz SNR - vanilla SNR"
            val_formatter = None

    def create_plot(results_file_path):
        gen = plot_measurement_results(
            results_file_path, val_formatter=val_formatter)
        fig, ax = next(gen)
        if axes_titles is not None:
            for idx in range(len(axes_titles)):
                ax[idx].set_title(axes_titles[idx])
        fig.suptitle(plot_title, fontsize="xx-large")
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        next(gen)
        plt.show()

    if data_processing_only:
        data_processing_pipeline_partial()
        return
    if measure_only:
        json_file_path = measurement_pipeline_partial(
            input_dir=measure_input_dir)
        create_plot(json_file_path)
        return

    if results_file_path is not None:
        create_plot(results_file_path)
        return

    data_processing_pipeline_partial()
    json_file_path = measurement_pipeline_partial(
        input_dir=output_dir)
    create_plot(json_file_path)


def create_parser(**kwargs) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(**kwargs)

    parser.add_argument(
        "-o", "--output-dir",
        dest="output_dir", required=False,
        help="Specify where in which directory to put output")

    parser.add_argument(
        "-c", "--config-file",
        dest="config_file", required=False,
        help="Specify the location of the RFI assessment configuration file")

    parser.add_argument(
        "-m", "--measure-only",
        dest="measure_only", action="store_true",
        help="Set this flag to run measurement pipeline without running DSPSR")

    parser.add_argument(
        "--measure-input-dir",
        dest="measure_input_dir", default=None,
        help=("Set this if the DSPSR archive files are "
              "not in the specified output directory"))

    parser.add_argument(
        "-d", "--data-processing-only",
        dest="data_processing_only", action="store_true",
        help=("Set this to create DSPSR archives "
              "without running measurement pipeline"))

    parser.add_argument(
        "-p", "--plot",
        dest="plot", default=None,
        help=("Plot some RFI assessment data that "
              "have been previously generated"))

    parser.add_argument(
        "-v", "--verbose",
        dest="verbose", action="store_true",
        help="Set log level to logging.DEBUG")

    return parser
