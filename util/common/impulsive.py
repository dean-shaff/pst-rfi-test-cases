import logging
import os
import typing

import partialize
import dspsr_test
import psr_formats

import util
import util.flatten
import util.chain
import util.psrchive_util
import util.stats_util

module_logger = logging.getLogger(__name__)


@partialize.partialize
async def measure_confusion(
    dspsr_dump_file_path: str,
    rfi_profile_file_path: str
) -> typing.Tuple[float]:
    """
    Get the true positive and false positive rate for a given simulated data
    set. This assumes that DSPSR has been setup to dump after the Spectral
    Kurtosis stage, and that we have access to the original RFI profile that
    was added to the simulated pulsar data.
    """
    dump_dada = psr_formats.DADAFile(dspsr_dump_file_path).load_data()
    rfi_prof_dada = psr_formats.DADAFile(rfi_profile_file_path).load_data()
    dump = dump_dada.data.copy()
    rfi_prof = rfi_prof_dada.data.copy()

    min_size = min([rfi_prof.shape[0], dump.shape[0]])
    rfi_prof = rfi_prof[:min_size, :, :]
    dump = dump[:min_size, :, :]

    # print(rfi_prof.shape)
    # print(dump.shape)

    true_pos = util.stats_util.calc_true_positive(rfi_prof, dump)
    false_pos = util.stats_util.calc_false_positive(rfi_prof, dump)

    return true_pos, false_pos


@partialize.partialize
async def measure_single(
    ar_file_path: str,
    *,
    freq_start: typing.Union[float, None],
    freq_end: typing.Union[float, None],
    on_pulse: typing.List[typing.List[int]],
    dm: typing.Union[float, None]
) -> float:
    """
    First zero all the channels outside the region of interest, the
    pre dedispere the data, then calculate the SNR of the data.
    """

    def format_psrstat_output(output):
        return output.split("=")[-1].strip()

    if freq_start is None and freq_end is None:
        pazi_file_path = ar_file_path
        psrplot_options = "freq+ -jpTD"
    else:
        pazi_cmd = await util.psrchive_util.generate_pazi_cmd(
            ar_file_path, freq_start, freq_end)

        await util.psrchive_util.get_cmd_output(pazi_cmd.format(ar_file_path))
        pazi_file_path = os.path.splitext(ar_file_path)[0] + ".pazi"
        psrplot_options = f"freq+ -jpTD -c y:win={freq_start}:{freq_end}"

    # make a plot of the data
    output_dir = os.path.dirname(pazi_file_path)
    output_template = os.path.basename(pazi_file_path)

    await dspsr_test.run_psrplot(
        pazi_file_path,
        plot_options=psrplot_options,
        output_template=output_template, output_dir=output_dir
    )

    # now do pam
    if dm is None:
        pam_cmd = f"pam -D -e dd {pazi_file_path}"
    else:
        pam_cmd = f"pam -d {dm} -D -e dd {pazi_file_path}"

    module_logger.debug(f"measure_single: pam_cmd={pam_cmd}")
    pam_file_path = os.path.splitext(ar_file_path)[0] + ".dd"

    await util.psrchive_util.get_cmd_output(pam_cmd)
    # now create custom SNR script to run with psrstat
    file_path = os.path.join(os.path.dirname(ar_file_path), "snr.psh")
    script_file_path = util.psrchive_util.generate_psrsh_snr_script(
        on_pulse, file_path=file_path)

    # make a plot of the data
    output_dir = os.path.dirname(pam_file_path)
    output_template = os.path.basename(pam_file_path)
    await dspsr_test.run_psrplot(
        pam_file_path,
        plot_options=f"flux -jpFT",
        output_template=output_template, output_dir=output_dir
    )

    # run psrstat with new script
    psrstat_cmd = f"psrstat -jpFT -C {script_file_path} -c snr {pam_file_path}"
    module_logger.debug(f"measure_single: psrstat_cmd={psrstat_cmd}")
    res = await util.psrchive_util.get_cmd_output(psrstat_cmd)
    chained = util.chain.chain(
        float, format_psrstat_output, lambda val: val.decode("utf-8"))
    snr = chained(res)
    module_logger.debug(f"measure_single: {ar_file_path}: snr={snr}")
    return snr
