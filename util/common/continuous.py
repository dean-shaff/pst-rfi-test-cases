import typing
import logging

import partialize
import psrchive
import numpy as np
import matplotlib.pyplot as plt

import util.psrchive_util
import util.stats_util


module_logger = logging.getLogger(__name__)

mask_array_type = typing.List[
    typing.List[typing.Union[int, float]]
]


@partialize.partialize
async def measure_single(
    ar_file_path: str,
    freq_or_chan_range: mask_array_type,
    rfi_freq_or_chan_range: mask_array_type
) -> typing.Tuple[float]:
    """
    Get the true and false positive ratio for continuous RFI sources.
    """

    def get_mask_from_freq_or_chan_range(archive, nchan, sub_range):

        if hasattr(sub_range[0], "is_integer"):
            chan_mask = np.arange(*[
                util.psrchive_util.freq_to_chan(archive, val)
                for val in sub_range
            ])
        else:
            chan_mask = np.arange(*sub_range)
        return chan_mask

    def reshape_archive_data(data):
        data_r = data.swapaxes(1, 2)
        subint, ndat, nchan = data_r.shape
        data_r = data_r.reshape((subint*ndat, nchan))
        data_r = data_r[:, :, np.newaxis]
        return data_r

    archive = psrchive.Archive_load(ar_file_path)
    archive.pscrunch()
    archive.dedisperse()
    nchan = archive.get_nchan()

    chan_mask = get_mask_from_freq_or_chan_range(
        archive, nchan, freq_or_chan_range)

    data = archive.get_data()[:, 0, :, :]  # subint, npol, nchan, ndat
    # data = dada.data[:, freq_mask, :]  # ndat, nchan, npol
    rfi_prof = np.zeros(data.shape)

    for sub_range in rfi_freq_or_chan_range:
        rfi_chan_mask = get_mask_from_freq_or_chan_range(
            archive, nchan, sub_range)
        rfi_prof[:, rfi_chan_mask, :] = 1.0

    data = data[:, chan_mask, :]
    rfi_prof = rfi_prof[:, chan_mask, :]

    data = reshape_archive_data(data)
    rfi_prof = reshape_archive_data(rfi_prof)

    true_pos = util.stats_util.calc_true_positive(rfi_prof, data, atol=1e-2, rtol=1e-2)
    false_pos = util.stats_util.calc_false_positive(rfi_prof, data, atol=1e-2, rtol=1e-2)

    module_logger.debug(f"measure_single: true_pos={true_pos}, false_pos={false_pos}")

    # imshow_kwargs = dict(
    #     aspect="auto",
    #     origin="lower")
    # fig, axes = plt.subplots(2, 1)
    # axes[0].imshow(rfi_prof[:, :, 0].T, **imshow_kwargs)
    # axes[1].imshow(data[:, :, 0].T, **imshow_kwargs)
    # plt.show()

    del archive

    return true_pos, false_pos


# @partialize.partialize
# async def measure_single(
#     ar_file_path: str,
#     chan_ranges: mask_array_type = None,
#     freq_ranges: mask_array_type = None
# ) -> float:
#     """
#     Determine what percentage of the channels in `chan_ranges` are equal to 0.0
#     """
#     archive = psrchive.Archive_load(ar_file_path)
#     archive.pscrunch()
#     archive.dedisperse()
#
#     if chan_ranges is None and freq_ranges is None:
#         raise RuntimeError("Need to provide either chan_ranges or freq_ranges")
#
#     if freq_ranges is not None:
#         chan_ranges = []
#         for sub_range in freq_ranges:
#             chan_ranges.append([
#                 util.psrchive_util.freq_to_chan(archive, freq)
#                 for freq in sub_range
#             ])
#
#     chan_mask = np.arange(*chan_ranges[0])
#     for chan_range in chan_ranges[1:]:
#         chan_mask = np.concatenate((
#             chan_mask,
#             chan_range
#         ))
#
#     data = archive.get_data()[:, 0, :, :]
#     module_logger.debug(f"measure_single: data.shape={data.shape}")
#
#     zeros = np.zeros((data.shape[0], chan_mask.shape[0], data.shape[-1]))
#     rfi_mask = np.zeros(data.shape[0], data.sha)
#
#     mean_iszero = np.mean(
#         np.isclose(data[:, chan_mask, :], zeros, atol=1e-2, rtol=1e-2))
#
#
#     module_logger.debug(f"measure_single: mean_iszero={mean_iszero}")
#     del archive
#
#     true_pos = util.stats_util.calc_true_positive(rfi_prof, dump)
#     false_pos = util.stats_util.calc_false_positive(rfi_prof, dump)
#
#     return true_pos, false_pos
#
#     return mean_iszero
