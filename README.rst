SKA PST RFI use cases
=====================

The purpose of the code in this repository is to assess the performance of the PST prototype's RFI mitigation algorithm on real and simulated datasets containing representative samples of RFI that will be seen at each of the SKA sites. The PST prototype, DSPSR, implements a family of RFI mitigation algorithms collectively called Spectral Kurtosis. For each SKA site, numerous types of RFI have been identified, usually corresponding to a specific family of RFI sources.

This repository consists of a set of configuration files that correspond to each RFI use case. These configuration files tell the Python assessment pipeline which performance metric to use. Which performance metric a given configuration will use depends on whether the input data is real or simulated, and whether the RFI use case is a continuous or impulsive source. Configuration files also indicate where in a set of data files to evaluate performance. For instance, a configuration file might tell the pipeline to look at a ten second slice of a dataset, while peering only at a 5 MHz window around some narrowband, impulsive source of RFI. The assessment pipeline is designed to assess the performance of the Spectral Kurtosis algorithm for a configurable range of algorithm parameters.

Performance Metrics
-------------------

Evaluating the performance of a RFI mitigation algorithm is made difficult by the fact that rarely is there a priori knowledge of the frequency/time position of RFI in a voltage dataset originating from a real radio astronomy instrument. Without this knowledge, it is impossible to measure the true and false positive rates of RFI detection. We handle this conundrum in two ways. First, when dealing with real datasets, we use a secondary metric, the signal to noise ratio, to assess the performance of the algorithm. Second, we work with datasets that have had artificial RFI injected. In the latter case, we are able to calculate true and false positive rates, at the risk of testing our algorithm on RFI that is not representative of that seen in the wild. For continuous sources of RFI, the situation is not as tricky, as we know that the algorithm should be flagging all samples in a specified sub-band of a dataset. Ultimately, we use two metrics to determine the performance of the Spectral Kurtosis algorithm:

- Signal to Noise Ratio (SNR)
- True/False positive rates


Configurations
--------------

RFI use case configurations are grouped into two categories: continuous and impulsive. For impulsive use cases, a configuration might use real or simulated data. For a more detailed analysis of these use cases, see `here <https://docs.google.com/document/d/1Vg-ohwqp2xv-WsgX1zliTx5exgTvhffSTn3EMLROVpw/edit?usp=sharing>`_.

All of the currently available configurations are in the ``configurations`` subdirectory of the project.

Continuous
~~~~~~~~~~

- Satellite (``satellite.toml``): For SKA Mid datasets, we focus on three frequency bands in the upper range of the Mid bandwidth. Uses True/False positive metric.
- Terrestrial (``terrestrial.toml``): For SKA Mid datasets, we focus on three frequency bands in the lower range of the Mid bandwidth. Uses True/False positive metric.

Impulsive
~~~~~~~~~

- ADS-B (``ADSB.toml``): 5 MHz wide RFI centered at 1090 MHz in SKA Mid datasets. Uses SNR metric.
- DME (``DME.toml``): 5 Mhz wide RFI centered at 1042.5 MHz in SKA Mid datasets. Uses SNR metric.
- Iridium Satellite Constellation (``iridium.toml``): A few bands of RFI in the 1615 - 1630 MHz range in SKA Mid datasets. Uses SNR metric.
- Simulated Pulsar/RFI (``simulated_pulsar.rfi.toml``): 5 MHz wide artificial RFI in simulated pulsar data. Uses true/false positive metric.


Usage
-----

.. code-block::

  poetry run python pipeline.py -c /path/to/configuration.toml -o /path/to/output/directory


For example:

.. code-block::

  poetry run python pipeline.py -c impulsive/ADSB.toml -o products/ADSB

This pipeline consists of three steps:

- running DSPSR on datasets with a variety of parameters
- measuring the performance of the SK algorithm
- plotting

We can run each of these steps individually, provided artefacts from previous steps are present:

.. code-block:: language

  poetry run python pipeline.py -c configurations/ADSB.toml -o products/ADSB -d
  poetry run python pipeline.py -c configurations/ADSB.toml -o products/ADSB -m
  poetry run python pipeline.py -o products/ADSB -p products/ADSB/measure.impulsive.real.json

Especially for configurations that use SKA Mid voltage data, these pipelines can take a long time to run. The first stage in particular is common to all those that use the same datasets. As a result, we can pipe artefacts from one pipeline run into another:

.. code-block::

  poetry run python pipeline.py -c impulsive/ADSB.toml -o products/ADSB
  poetry run python pipeline.py -c impulsive/DME.toml --measure-input-dir products/ADSB -o products/DME -m

This will use the archive files generated in the first pipeline run to do measurements for the second configuration.

Writing new configurations
~~~~~~~~~~~~~~~~~~~~~~~~~~

Say we'd like to investigate some new source of impulsive RFI present in some MeerKAT data. We can write a configuration file that will measure the change in SNR due to the Spectral Kurtosis algorithm for this source of RFI. Say, for instance that the RFI is centred at 1200 MHz, and we'd like to look at a 5 MHz band on either side of that centre frequency. In order to reduce the amount of time spent creating archive files, we decide to limit ourselves to looking at a five second segment of the input data:


.. code-block:: toml

  type="impulsive"

  data_dir="/path/to/meerkat/data"
  dspsr_bin="/path/to/dspsr/executable"

  [processing]
  options = "-D 0 -cuda 0" # use GPU
  skzm = [64, 128, 256, 512, 1024, 2048, 4096, 8192]
  skzs = [3, 4, 5]
  skz_no_fscr = true # don't use SK fscr sub implementation
  time_range = [6, 11] # only look at five seconds of data
  time_interval = 1 # dump archives every one second

  [measurement]
  freq_range = [1195, 1205] # look at a 10 MHz band centred at 1200 Mhz
  on_pulse = [
    [0.1, 0.18] # we know that the pulsar is located in this phase range in the output archive
  ]

When we run this configuration through the pipeline, it will ultimately create a heat map of the SK-induced change in SNR for the range of SK ``M`` and ``s`` values specified in the ``processing.skzm`` and ``processing.skzs`` fields.

.. Obtaining Test Data
.. ~~~~~~~~~~~~~~~~~~~
..
.. Right now, these configurations are set up to run in a specific environment on a shared server. In order to run them locally, in addition to installing the package itself, we'd have to acquire the data used by the pipeline.
..

Installation
------------

This project is Python 3.6+.

With [poetry](https://github.com/sdispater/poetry) installed:

.. code-block::

  poetry install


Testing
-------

This repo contains a small suite of test cases for the utility code used to run RFI assessment pipelines. To run these tests:


.. code-block::

  poetry run python -m unittest
