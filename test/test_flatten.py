import unittest

from util import flatten


class TestFlatten(unittest.TestCase):

    def test_flatten(self):

        lists = [
            list("my"),
            list("name"),
            list("is")
        ]

        expected_val = []
        for ai in lists[0]:
            for bi in lists[1]:
                for ci in lists[2]:
                    expected_val.append((ai, bi, ci))

        test_val = list(flatten.flatten(*lists))
        self.assertTrue(test_val == expected_val)

    def test_flatten_1d(self):
        lists = [list("the cat is")]

        expected_val = [(val, ) for val in lists[0]]

        test_val = list(flatten.flatten(*lists))

        self.assertTrue(test_val == expected_val)

    def test_flatten_2d(self):

        lists = [
            list("my"),
            list("name")
        ]

        expected_val = []
        for ai in lists[0]:
            for bi in lists[1]:
                expected_val.append((ai, bi))

        test_val = list(flatten.flatten(*lists))
        self.assertTrue(test_val == expected_val)


if __name__ == "__main__":
    unittest.main()
