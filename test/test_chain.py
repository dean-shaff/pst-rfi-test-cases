import unittest

from util import chain


class TestChain(unittest.TestCase):

    def test_chain(self):

        chained = chain.chain(float, int)

        self.assertTrue(chained("5") == 5.0)


if __name__ == "__main__":
    unittest.main()
